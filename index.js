/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	let printuserInfo = function printUsers(){
		let userfullName = prompt("What is your name?"); 
		let userAge = prompt("How old are you?"); 
		let userLocation = prompt("Where do you live?");

		
		console.log("Hello," + userfullName); 
		console.log("You are " + userAge + " years old"); 
		console.log("You live in " + userLocation); 
	};

	printuserInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	function diplayTopFiveMusic() {
		console.log("1. Beatles");
		console.log("2. Metallica");
		console.log("3. The Egles");
		console.log("4. L'arc~en~Ciel");
		console.log("5. Eraserheads");

	};

	//second function here:
	diplayTopFiveMusic();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	function diplayTopFiveMovies() {
		console.log("1. Avangers End Game");
		console.log("Rotten Tomatoes Rating: 94%");
		console.log("2. Avangers Infinity War");
		console.log("Rotten Tomatoes Rating: 85%");
		console.log("3. Guardians of the Galaxy");
		console.log("Rotten Tomatoes Rating: 92%");
		console.log("4. One Piece Film Red");
		console.log("Rotten Tomatoes Rating: 93%");
		console.log("5. Thor: Ragnarok");
		console.log("Rotten Tomatoes Rating: 93%");

	};
	//third function here:
	diplayTopFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);
printFriends();